import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DataHandlerService } from '../../../service/data-handler.service';
import { Task } from '../../model/Task';
import { Category } from '../../model/Category';
import { Priority } from '../../model/Priority';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { OperType } from '../OperType';


@Component({
  selector: 'app-edit-task-dialog',
  templateUrl: './edit-task-dialog.component.html',
  styleUrls: ['./edit-task-dialog.component.css']
})
export class EditTaskDialogComponent implements OnInit {

  constructor(
              private dialogRef: MatDialogRef<EditTaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: [Task, string, OperType],
              private dataHandler: DataHandlerService,
              private dialog: MatDialog
             ) { }

  dialogTitle: string;
  task: Task;
  tmpTitle: string;
  tmpCategory: Category;
  tmpPriority: Priority;
  categories: Category[];
  priority: Priority[];
  tmpDate: Date;
  isNew: OperType;

  ngOnInit(): void {
    this.task = this.data[0];
    this.dialogTitle = this.data[1];
    this.tmpTitle = this.task.title;
    this.tmpCategory = this.task.category;
    this.tmpDate = this.task.date;
    this.tmpPriority = this.task.priority;
    this.isNew = this.data[2];

    this.dataHandler.getCategories().subscribe(items => this.categories = items);
    this.dataHandler.getAllPriorities().subscribe(items => this.priority = items);
  }

  onConfirm(): void {
    this.task.title = this.tmpTitle;
    this.task.category = this.tmpCategory;
    this.task.priority = this.tmpPriority;
    this.task.date = this.tmpDate;
    this.dialogRef.close(this.task);
  }

  onCansel(): void {
    this.dialogRef.close(null);
  }

  onDelete() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
          dataTitle: 'Confirm your action',
          message: `Are you sour want you wont delete this task: "${this.task.title}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.dialogRef.close('delete');
       }
    });
  }

  actualize(): void {
    this.dialogRef.close('actualize');
  }

  complete() {
    this.dialogRef.close('complete');
  }

  checkIsNew() {
    return this.isNew === OperType.EDIT;
  }
}
