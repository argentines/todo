import { Component, Inject, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { Task } from '../../model/Task';
import { Category } from '../../model/Category';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DataHandlerService } from '../../../service/data-handler.service';
import { OperType } from '../OperType';

@Component({
  selector: 'app-edit-category-dialog',
  templateUrl: './edit-category-dialog.component.html',
  styleUrls: ['./edit-category-dialog.component.css']
})
export class EditCategoryDialogComponent implements OnInit {

  dialogTitle: string;
  categoryTitle: string;
  canDelete: OperType;

  constructor(
              private dialogRef: MatDialogRef<EditCategoryDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: [string, string, OperType],
              private dataHandler: DataHandlerService,
              private dialog: MatDialog
              ) { }

  ngOnInit(): void {
    this.categoryTitle = this.data[0];
    this.dialogTitle = this.data[1];
    this.canDelete = this.data[2];

    /*if (!this.categoryTitle) {
      this.canDelete = false;
    }*/
  }

  onConfirm(): void {
    this.dialogRef.close(this.categoryTitle);
  }

  onCansel(): void {
    this.dialogRef.close(false);
  }

  onDelete() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this category: "${this.categoryTitle}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dialogRef.close('delete');
      }
    });
  }

  actualize(): void {
    this.dialogRef.close('actualize');
  }

}
