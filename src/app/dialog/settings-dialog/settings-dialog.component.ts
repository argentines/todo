import { Component, OnInit } from '@angular/core';
import { Priority } from '../../model/Priority';
import { MatDialogRef } from '@angular/material/dialog';
import { DataHandlerService } from '../../../service/data-handler.service';

@Component({
  selector: 'app-settings-dialog',
  templateUrl: './settings-dialog.component.html',
  styleUrls: ['./settings-dialog.component.css']
})
export class SettingsDialogComponent implements OnInit {

  priorities: Priority[];

  constructor(private dialogRef: MatDialogRef<SettingsDialogComponent>,
              private dataHandlerService: DataHandlerService) { }

  ngOnInit(): void {
    this.dataHandlerService.getAllPriorities().subscribe(priorities => this.priorities = priorities);
  }

  onClouse(): void {
    this.dialogRef.close(false);
  }

  onAddPriority(priority: Priority) {
    this.dataHandlerService.addPriority(priority).subscribe();
  }

  onUpdatePriority(priority: Priority) {
    this.dataHandlerService.updatePriority(priority).subscribe();
  }

  onDeletePriority(priority: Priority) {console.log(priority.id);
    this.dataHandlerService.deletePriority(priority.id).subscribe();
  }
}
