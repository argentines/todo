import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {Priority} from '../../model/Priority';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {OperType} from '../OperType';
import {DataHandlerService} from '../../../service/data-handler.service';

@Component({
  selector: 'app-edit-priority-dialog',
  templateUrl: './edit-priority-dialog.component.html',
  styleUrls: ['./edit-priority-dialog.component.css']
})
export class EditPriorityDialogComponent implements OnInit {

  dialogTitle: string;
  priorityTitle: string;
  priority: Priority;
  operType: OperType;

  constructor(private dialogRef: MatDialogRef<EditPriorityDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: [string, string, OperType],
              private dataHandler: DataHandlerService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.priorityTitle = this.data[0];
    this.dialogTitle = this.data[1];
    this.operType = this.data[2];
  }

  onConfirm() {
    this.dialogRef.close(this.priorityTitle);
  }

  onCansel() {
    this.dialogRef.close(false);
  }

  onDelete() {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this priority: "${this.priority.title}?"`
      },
      autoFocus: false
    });

    dialog.afterClosed().subscribe(result => {

      if (result) {
        this.dialogRef.close('delete');
      }
    });
  }

  canDelete(): boolean {
    return this.operType === OperType.EDIT;
  }
}
