import { Component, OnInit } from '@angular/core';
import { Task } from './model/Task';
import { DataHandlerService } from '../service/data-handler.service';
import { Category } from './model/Category';
import { Priority } from './model/Priority';
import { Observable, of, pipe, zip} from 'rxjs';
import { concatMap, map} from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'todo';
  tasks: Task[];
  categories: Category[];
  priorities: Priority[];
  selectedCategory: Category = null;
  searchTaskText = '';
  statusFilter: boolean;
  priorityFilter: Priority;
  searchCategoryText = '';
  toggleStatistic = true;
  categoryMap = new Map<Category, number>();
  menuOpened: boolean;
  menuMode: string;
  menuPosition: string;
  showBackdrop: boolean;
  isMobile: boolean;
  isTablet: boolean;

  totalTasksCountInCategory: number;
  completedCountInCategory: number;
  uncompletedCountInCategory: number;
  uncompletedTotalTasksInCount: number;

  constructor(private dataHandlerService: DataHandlerService, private deviceService: DeviceDetectorService) {
    
    this.isMobile = deviceService.isMobile();
    this.isTablet = deviceService.isTablet();
    this.setMenuDisplayParams();
  }

  ngOnInit() {
    this.dataHandlerService.getCategories().subscribe(category => this.categories = category);
    this.dataHandlerService.getAllPriorities().subscribe(items => this.priorities = items);
    this.fillCatigory();
    this.onSelectCategory(null);
  }

  // Categories
  fillCatigory() {
    if (this.categoryMap) {
      this.categoryMap.clear();
    }

    this.categories = this.categories.sort((a, b) => a.title.localeCompare(b.title));

    this.categories.forEach(cat => {
      this.dataHandlerService.getUncompletedCountInCategory(cat).subscribe(count => this.categoryMap.set(cat, count));
    });
  }

  onSelectCategory(category: Category) {
    this.selectedCategory = category;
    this.updateTaskAndStatus();
  }

  onUpdateCategory(category: Category) {
    this.dataHandlerService.updateCategory(category).subscribe(() => {
      this.onSelectCategory(this.selectedCategory);
    });
  }

  onDeleteCategory(category: Category) {

    this.dataHandlerService.deleteCategory(category.id).subscribe(cat => {
      this.selectedCategory = null;
      this.onSelectCategory(null);
      this.categoryMap.delete(cat);
      this.updateTasks();
    });
  }

  onAddCategory(title: string) {
    this.dataHandlerService.addCategory(title).subscribe(() => {
      this.fillCatigory();
    });
  }

  private updateCategory() {
    this.categories.forEach(cat => {
      this.dataHandlerService.getCategories().subscribe(categories => this.categories = categories);
    });
  }

  onSearchCategory(category: string) {

    this.searchCategoryText = category;
    this.dataHandlerService.searchCategory(category).subscribe(categories => {
      this.categories = categories;
      this.fillCatigory();
    });
  }

  // Task

  onUpdateTask(task: Task) {
    this.dataHandlerService.updateTask(task).subscribe(() => {
      this.fillCatigory();
      this.updateTaskAndStatus();
    });
  }

  onDeleteTask(task: Task) {
    this.dataHandlerService.deleteTask(task.id).pipe(
      concatMap(task => {
         return this.dataHandlerService.getUncompletedCountInCategory(task.category).pipe(map(count => {
                    return ({t: task, count});
         }));
      })
    ).subscribe(result => {
      const t = result.t as Task;
      this.categoryMap.set(t.category, result.count);

      this.updateTaskAndStatus();
    });
  }

  onSearchByTitle(searchString: string) {
    this.searchTaskText = searchString;
    this.updateTasks();
  }

  onFilterTaskByStatus(status: boolean) {
    this.statusFilter = status;
    this.updateTasks();
  }

  onFilterTaskByPriority(priority: Priority) {
    this.priorityFilter = priority;
    this.updateTasks();
  }

  updateTasks() {
    this.dataHandlerService.searchTask(
      this.selectedCategory,
      this.searchTaskText,
      this.statusFilter,
      this.priorityFilter
    ).subscribe((tasks: Task[]) => {
        this.tasks = tasks;
    });
  }

  onAddTask(task: Task) {
   this.dataHandlerService.addTask(task).pipe(
      concatMap(task => {
        return this.dataHandlerService.getUncompletedCountInCategory(task.category).pipe(map(count => {
          return ({t: task, count});
        }));
      })
    ).subscribe(result => {
       const t = result.t as Task;
       if (t.category) {
         this.categoryMap.set(t.category, result.count);
       }

       this.updateTaskAndStatus();
    });
  }

  updateTaskAndStatus() {
    this.updateTasks();
    this.updateStat();
  }

  private updateStat() {

    const getTotalCountInCategory$ = this.dataHandlerService.getTotalCountInCategory(this.selectedCategory);
    const getCompletedCountInCategory$ = this.dataHandlerService.getCompletedCountInCategory(this.selectedCategory);
    const getUncompletedCountInCategory$ = this.dataHandlerService.getUncompletedCountInCategory(this.selectedCategory);
    const getUncompletedTotalCount$ = this.dataHandlerService.getUncompletedTotalCount();

    zip(
        getTotalCountInCategory$,
        getCompletedCountInCategory$,
        getUncompletedCountInCategory$,
        getUncompletedTotalCount$
    ).pipe(
        map(([getTotalCountInCategory,
                    getCompletedCountInCategory,
                    getUncompletedCountInCategory,
                    getUncompletedTotalCount]) => ({
                        getTotalCountInCategory,
                        getCompletedCountInCategory,
                        getUncompletedCountInCategory,
                        getUncompletedTotalCount
                    }))
    ).subscribe(all => {
        this.totalTasksCountInCategory = all.getTotalCountInCategory,
        this.completedCountInCategory = all.getCompletedCountInCategory,
        this.uncompletedCountInCategory = all.getUncompletedCountInCategory,
        this.uncompletedTotalTasksInCount = all.getUncompletedTotalCount;
      });
  }

  toggleStat(toggle: boolean) {
    this.toggleStatistic = toggle;
  }

  setMenuDisplayParams() {
    this.menuPosition = 'left'; 

    if (this.isMobile) {
      this.menuOpened = false;
      this.menuMode = 'over'; 
      this.showBackdrop = true;
    } else {
      this.menuOpened = true;
      this.menuMode = 'push';
      this.showBackdrop = false;
    }

  }

  onClosedMenu() {
    this.menuOpened = false;
  }

  toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }
}

