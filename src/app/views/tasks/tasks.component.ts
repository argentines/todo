import { AfterViewInit, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DataHandlerService } from '../../../service/data-handler.service';
import {  MatDialog } from '@angular/material/dialog';
import { Task } from 'src/app/model/Task';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { EditTaskDialogComponent } from '../../dialog/edit-task-dialog/edit-task-dialog.component';
import { ConfirmDialogComponent } from '../../dialog/confirm-dialog/confirm-dialog.component';
import { Priority } from '../../model/Priority';
import { Category } from '../../model/Category';
import { OperType } from '../../dialog/OperType';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  public dataSource: MatTableDataSource<Task>;
  public displayedColumns: string[] = ['color', 'id', 'title', 'date', 'priority', 'category', 'operations', 'select'];

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;

  tasks: Task[];
  priorities: Priority[];
  searchTaskText = '';
  selectedStatusFilter: boolean;
  selectedPriorityFilter: Priority;
  isMobile: boolean;
  isTablet: boolean;

  @Input('tasks')
  private set setTasks(tasks: Task[]) {
    this.tasks = tasks;
    this.fillTable();
  }

  @Input('priorities')
  private set setPriority(priorities: Priority[]) {
    this.priorities = priorities;
    this.fillTable();
  }

  @Output()
  updateTask = new EventEmitter<Task>();

  @Output()
  deleteTask = new EventEmitter<Task>();

  @Output()
  selectCategory = new EventEmitter<Category>();

  @Output()
  filterByTitle = new EventEmitter<string>();

  @Output()
  filterByStatus = new EventEmitter<boolean>();

  @Output()
  filterByPriority = new EventEmitter<Priority>();

  @Output()
  addTask = new EventEmitter<Task>();

  @Input()
  selectedCategory: Category;

  constructor(private dataHandler: DataHandlerService,
              private dialog: MatDialog,
              private diviceServise: DeviceDetectorService) {

                this.isMobile = diviceServise.isMobile();
                this.isTablet = diviceServise.isTablet();
               }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.fillTable();
  }

  private fillTable() {

    if (!this.dataSource) {
      return;
    }

    this.dataSource.data = this.tasks;
    this.addTableObject();

    this.dataSource.sortingDataAccessor = (task, colName) => {

      switch (colName) {
        case 'priority': {
            return task.priority ? task.priority.id : null;
        }
        case 'category': {
            return task.category ? task.category.title : null;
        }
        case 'date': {
            return task.date ? task.date.toString() : null;
        }
        case 'title': {
            return task.title;
        }
      }
    };
  }

  public getPriorityColor(task: Task): string{

    if (task.completed) {
      return '#F8F9FA';
    }

    if (task.priority && task.priority.color) {
      return task.priority.color;
    }

    return '#fff';
  }

  public addTableObject() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public editTaskDialog(task: Task) {

    const dialogRef = this.dialog.open(EditTaskDialogComponent, {
          data: [task, 'Edit Task', OperType.EDIT],
          autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {

        if (result === 'delete') {
          this.deleteTask.emit(task);
          return;
        }

        if (result === 'complete') {
          task.completed = true;
          this.updateTask.emit(task);
          return;
        }

        if (result === 'actualize') {
          task.completed = false;
          this.updateTask.emit(task);
          return;
        }

        if (result as Task) {
          this.updateTask.emit(task);
          return;
        }
    });
  }

  openDeleteDialog(task: Task) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this task: "${task.title}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteTask.emit(task);
      }
    });
  }

  onToggleStatus(task: Task) {
    task.completed = !task.completed;
    this.updateTask.emit(task);
  }

  onSelectCategory(category: any) {
    this.selectCategory.emit(category);
  }

  onFilterByTitle() {
    this.filterByTitle.emit(this.searchTaskText);
  }

  onFilterByStatus(value: boolean) {

    if (value !== this.selectedStatusFilter) {
        this.selectedStatusFilter = value;
        this.filterByStatus.emit(this.selectedStatusFilter);
    }
  }

  onFilterByPriority(priority: Priority) {

    if (priority !== this.selectedPriorityFilter) {
      this.selectedPriorityFilter = priority;
      this.filterByPriority.emit(this.selectedPriorityFilter);
    }
  }

  addNewTask() {
    const task = new Task(null, '', false, null, this.selectedCategory);

    const dialogRef = this.dialog.open(EditTaskDialogComponent, {data: [task, 'Add Task', OperType.ADD]});
    dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.addTask.emit(task);
       }
    });
  }

  getMobilePriorityBgColor(task: Task): string {

    if (task.priority != null && !task.completed) {
      return task.priority.color;
    }

    return 'none';
  }
}
