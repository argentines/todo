import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SettingsDialogComponent } from '../../dialog/settings-dialog/settings-dialog.component';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input()
  categoryName: string;

  @Output()
  toggleStat = new EventEmitter<boolean>();

  @Input()
  toggleStatistic: boolean;

  @Output()
  toggleMenu = new EventEmitter();

  isMobile: boolean;
  isTablet: boolean;

  constructor(private dialog: MatDialog, deviceServise: DeviceDetectorService) { 
    this.isMobile = deviceServise.isMobile();
    this.isTablet = deviceServise.isTablet();
  }

  ngOnInit() {

  }

  toggleState() {
    this.toggleStat.emit(!this.toggleStatistic);
  }

  showSettings() {
    const dialogRef = this.dialog.open(SettingsDialogComponent, {
       autoFocus: false,
       width: '500px'
    });
  }

  onToggleMenu() {
    this.toggleMenu.emit();
  }
}
