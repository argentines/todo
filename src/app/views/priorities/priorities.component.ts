import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Priority } from '../../model/Priority';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../dialog/confirm-dialog/confirm-dialog.component';
import { EditPriorityDialogComponent } from '../../dialog/edit-priority-dialog/edit-priority-dialog.component';
import { OperType } from '../../dialog/OperType';

@Component({
  selector: 'app-priorities',
  templateUrl: './priorities.component.html',
  styleUrls: ['./priorities.component.css']
})
export class PrioritiesComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  static defaultColor = '#fff';


  @Input()
  priorities: [Priority];

  @Output()
  addPriority = new EventEmitter<Priority>();

  @Output()
  updatePriority = new EventEmitter<Priority>();

  @Output()
  deletePriority = new EventEmitter<Priority>();


  ngOnInit(): void {
  }

  setColor(priority: Priority, color: string) {
    priority.color = color;
  }

  onAddPriority() {

    const dialogRef = this.dialog.open(EditPriorityDialogComponent, {
        data: ['', 'Add new Priority', OperType.ADD]
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        const newPriority = new Priority(null, result as string, PrioritiesComponent.defaultColor);
        this.addPriority.emit(newPriority);
      }
    });
  }

  onDeletePriority(priority: Priority) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        dataTitle: 'Confirm your action',
        message: `Are you sour want you wont delete this priority: "${priority.title}?"`
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.deletePriority.emit(priority);
      }
    });
  }

  onUpdatePriority(priority: Priority) {

    const dialogRef = this.dialog.open(EditPriorityDialogComponent, {

      data: [priority.title, 'Edit Priority', OperType.EDIT],
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === 'delete') {
        this.deletePriority.emit(priority);
        return;
      }

      if (result) {
        priority.title = result as string;
        this.updatePriority.emit(priority);
        return;
      }
    });
  }
}
