import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AboutDialogComponent} from "../../dialog/about-dialog/about-dialog.component";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  year: Date;
  blog: 'http://work-todo.com';
  site: 'http://work-todo.com/blog';
  siteName: 'Todo';

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
    this.year = new Date();
  }

  onAbouteDialog() {
    this.dialog.open(AboutDialogComponent, {
      autoFocus: false,
      data: {
        dialogTitle: 'About programma',
        message: 'This programm was create by help you chech your beasnes.'
      },
      width: '400px'
    });
  }
}
