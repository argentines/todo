import { Component, OnInit, Input } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-stat',
  templateUrl: './stat.component.html',
  styleUrls: ['./stat.component.css']
})
export class StatComponent implements OnInit {

  @Input()
  completeTasksInCategory: number;

  @Input()
  totalTasksInCategory: number;

  @Input()
  uncompleteTasksInCategory: number;

  @Input()
  toggleStatistic: boolean;

  isMobile: boolean;
  isTablet: boolean;

  constructor(deviceServise: DeviceDetectorService) {
    this.isMobile = deviceServise.isMobile();
    this.isTablet = deviceServise.isTablet();
  }

  ngOnInit(): void {
  }

}
