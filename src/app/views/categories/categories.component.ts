import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { DataHandlerService } from '../../../service/data-handler.service';
import { Category } from '../../model/Category';
import { MatDialog } from '@angular/material/dialog';
import { EditCategoryDialogComponent } from '../../dialog/edit-category-dialog/edit-category-dialog.component';
import { OperType } from '../../dialog/OperType';
import { DeviceDetectorService } from 'ngx-device-detector';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  selectedCategoryMap: Map<Category, number>;
  isMobile: boolean;
  isTablet: boolean;

  constructor(private dataHandler: DataHandlerService, 
              private dialog: MatDialog,
              private diviceServise: DeviceDetectorService) { 

                this.isMobile = this.diviceServise.isMobile();
                this.isTablet = this.diviceServise.isTablet();
              }

  @Input()
  categories: Category[];

  @Input()
  selectedCategory: Category;

  @Input()
  uncompletedTotal: number;

  @Input('categoryMap')
  set setCategoryMap(categoryMap: Map<Category, number>) {
    this.selectedCategoryMap = categoryMap;
  }

  @Output()
  selectCategory = new EventEmitter<Category>();

  @Output()
  updateCategory = new EventEmitter<Category>();

  @Output()
  deleteCategory = new EventEmitter<Category>();

  @Output()
  addCategory = new EventEmitter<string>();

  @Output()
  searchCategory = new EventEmitter<string>();

  indexMouseMove: number;
  searchCategoryTitle: string;

  ngOnInit(): void {}

  showTaskByCategory(category: Category): void{

    if (this.selectedCategory === category) {
      return;
    }
    this.selectedCategory = category;
    this.selectCategory.emit(this.selectedCategory);
  }

  showEditIcon(index: number): void {
    this.indexMouseMove = index;
  }

  openEditDialog(category: Category) {

    const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
       data: [category.title, 'Edit Category', OperType.EDIT],
       width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === 'delete') {
        this.deleteCategory.emit(category);
        return;
      }

      if (typeof(result) === 'string') {
        category.title = result as string;

        this.updateCategory.emit(category);
        return;
      }
    });
  }

  addNewCatigory() {

    const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
        data: ['', 'Add Category', OperType.ADD],
        width: '400px'
     });

    dialogRef.afterClosed().subscribe(result => {
         if (result) {
           this.addCategory.emit(result as string);
         }
     });
  }

  search() {

    if (this.searchCategoryTitle === null) {
      return;
    }
    this.searchCategory.emit(this.searchCategoryTitle);
  }
}
