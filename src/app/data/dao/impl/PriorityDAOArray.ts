import { PriorityDAO } from '../interface/PriorityDAO';
import { Observable, of } from 'rxjs';
import { Priority } from '../../../model/Priority';
import { TestData } from '../../TestData';

export class PriorityDAOArray implements PriorityDAO{

  static priorities = TestData.priorities;

  add(priority: Priority): Observable<Priority> {

    if (priority.id === null || priority.id === 0) {
      priority.id = this.getLastPriorityId();
    }
    PriorityDAOArray.priorities.push(priority);

    return of(priority);
  }

  delete(id: number): Observable<Priority> {

    TestData.tasks.forEach(task => {
       if (task.priority && task.priority.id === id) {
         task.priority = null;
         console.log('before if');
       }
       console.log('after If');
    });

    const priority = TestData.priorities.find(t => t.id === id);
    TestData.priorities.splice(TestData.priorities.indexOf(priority), 1);
    return of(priority);
  }

  get(id: number): Observable<Priority> {
    return undefined;
  }

  getAll(): Observable<Priority[]> {
    return of(TestData.priorities);
  }

  update(priority: Priority): Observable<Priority> {
    const tmpPriority = TestData.priorities.find(p => p.id === priority.id);
    TestData.priorities.splice(TestData.priorities.indexOf(tmpPriority), 1, tmpPriority);
    return of(tmpPriority);
  }

  getLastPriorityId() {
    return Math.max.apply(Math, TestData.priorities.map(priority => priority.id)) + 1;
  }
}
