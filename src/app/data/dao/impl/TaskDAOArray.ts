import {TaskDAO} from '../interface/TaskDAO';
import {Observable, of} from 'rxjs';
import {Category} from '../../../model/Category';
import {Priority} from '../../../model/Priority';
import { Task } from 'src/app/model/Task';
import {TestData} from '../../TestData';

export class TaskDAOArray implements TaskDAO{

  add(task: Task): Observable<Task> {

      if (task.id === null || task.id === 0) {
          task.id = this.getLastTaskId();
      }
      TestData.tasks.push(task);

      return of(task);
  }

  delete(id: number): Observable<Task> {
    const task = TestData.tasks.find(t => t.id === id);
    TestData.tasks.splice(TestData.tasks.indexOf(task), 1);
    return of(task);
  }

  get(id: number): Observable<Task> {
    return undefined;
  }

  getAll(): Observable<Task[]> {
    return of(TestData.tasks);
  }

  getCompletedCountInCategory(category: Category): Observable<number> {
    return of(this.searchTodos(category, null, true, null).length);
  }

  getTotalCount(): Observable<number> {
    return of(TestData.tasks.length);
  }

  getTotalCountInCategory(category: Category): Observable<number> {
    return of(this.searchTodos(category, null, null, null).length);
  }

  getUncompletedCountInCategory(category: Category): Observable<number> {
    return of(this.searchTodos(category, null, false, null).length);
  }

  search(category: Category, searchText: string, status: boolean, priority: Priority): Observable<Task[]> {
    return of(this.searchTodos(category, searchText, status, priority));
  }

  update(task: Task): Observable<Task> {
    const tmpTask = TestData.tasks.find(t => t.id === task.id);
    TestData.tasks.splice(TestData.tasks.indexOf(tmpTask), 1, task);
    return of(task);
  }

  private searchTodos(category: Category, searchText: string, status: boolean, priority: Priority): Task[] {

    let allTasks = TestData.tasks;

    if (category != null) {
      allTasks = allTasks.filter(todo => todo.category === category);
    }

    if (searchText != null) {
      allTasks = allTasks.filter(todo => todo.title.toUpperCase().includes(searchText.toUpperCase()));
    }

    if (status != null) {
      allTasks = allTasks.filter(todo => todo.completed === status);
    }

    if (priority != null) {
      allTasks = allTasks.filter(todo => todo.priority === priority);
    }

    return allTasks;
  }

  getLastTaskId(): number {
    return Math.max.apply(Math, TestData.tasks.map(task => task.id)) + 1;
  }
}
