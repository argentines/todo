import { Injectable } from '@angular/core';
import { Category } from '../app/model/Category';
import { Task } from '../app/model/Task';
import { Observable, of } from 'rxjs';
import { TaskDAOArray } from '../app/data/dao/impl/TaskDAOArray';
import { CategoryDAOArray } from '../app/data/dao/impl/CategoryDAOArray';
import { Priority } from '../app/model/Priority';
import { PriorityDAOArray } from '../app/data/dao/impl/PriorityDAOArray';


@Injectable({
  providedIn: 'root'
})
export class DataHandlerService {

  private taskDAOArray = new TaskDAOArray();
  private categoryDaoArray = new CategoryDAOArray();
  private priorityDaoArray = new PriorityDAOArray();

  constructor() { }

  getAllTasks(): Observable<Task[]> {
    return this.taskDAOArray.getAll();
  }

  searchTask(category: Category, searchText: string, status: boolean, priority: Priority): Observable<Task[]> {
    return this.taskDAOArray.search(category, searchText, status, priority);
  }

  addTask(task: Task): Observable<Task> {
    return this.taskDAOArray.add(task);
  }

  updateTask(task: Task): Observable<Task> {
    return this.taskDAOArray.update(task);
  }

  deleteTask(id: number): Observable<Task> {
    return this.taskDAOArray.delete(id);
  }



  getCategories(): Observable<Category[]> {
    return this.categoryDaoArray.getAll();
  }

  searchCategory(category: string): Observable<Category[]> {
    return this.categoryDaoArray.search(category);
  }

  updateCategory(category: Category): Observable<Category> {
    return this.categoryDaoArray.update(category);
  }

  deleteCategory(id: number): Observable<Category> {
    return this.categoryDaoArray.delete(id);
  }

  addCategory(title: string): Observable<Category> {
    return this.categoryDaoArray.add(new Category(null, title));
  }



  getTotalCountInCategory(category: Category): Observable<number> {
    return this.taskDAOArray.getTotalCountInCategory(category);
  }

  getCompletedCountInCategory(category: Category): Observable<number> {
    return this.taskDAOArray.getCompletedCountInCategory(category);
  }

  getUncompletedCountInCategory(category: Category): Observable<number> {
    return this.taskDAOArray.getUncompletedCountInCategory(category);
  }

  getUncompletedTotalCount(): Observable<number> {
    return this.taskDAOArray.getUncompletedCountInCategory(null);
  }


  getAllPriorities(): Observable<Priority[]> {
    return this.priorityDaoArray.getAll();
  }

  addPriority(priority: Priority): Observable<Priority> {
    return this.priorityDaoArray.add(priority);
  }

  updatePriority(priority: Priority): Observable<Priority> {
    return this.priorityDaoArray.update(priority);
  }

  deletePriority(id: number): Observable<Priority> {
    return this.priorityDaoArray.delete(id);
  }
}
